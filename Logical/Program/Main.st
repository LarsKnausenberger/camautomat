
PROGRAM _INIT

	fbMpAxisBasic_Master.MpLink := ADR(gAxMaster);
	fbMpAxisBasic_Master.Parameters := ADR(MasterPar);
	fbMpAxisBasic_Master(Enable := TRUE);
	
	fbMpAxisBasic_Slave.MpLink := ADR(gAxSlave);
	fbMpAxisBasic_Slave.Parameters := ADR(SlavePar);
	fbMpAxisBasic_Slave(Enable := TRUE);
	
	CamSections.Section[0].MasterPoint := 0.0;
	CamSections.Section[0].SlavePoint := 0.0;
	CamSections.Section[0].Mode := mcCAMSECTREF_ABSOLUTE;
	CamSections.Section[0].Type := mcCAMSECTLAW_5TH_ORDER_POLYNOM;
	
	CamSections.Section[1].MasterPoint := 360000.0;
	CamSections.Section[1].SlavePoint := 180000.0;
	CamSections.Section[1].Mode := mcCAMSECTREF_ABSOLUTE;
	CamSections.Section[1].Type := mcCAMSECTLAW_LAST_POINT;	
	
	fbCamPrepare.Cam.DataObjectName := '';
	fbCamPrepare.CamID := 1;
	
	fbCamAutomatGetPar.CamAutomat.DataObjectName := 'Feature_CamAutomat';
	fbCamAutomatGetPar.CamAutomat.DataSize := SIZEOF(CamAutPar);
	fbCamAutomatGetPar.Command :=  mcGET_PAR_FROM_OBJECT;
	
	fbCamAutomatSetPar.Command := mcSET_ALL_PAR_FROM_ADR;
	
END_PROGRAM

PROGRAM _CYCLIC

	fbMpAxisBasic_Master.MpLink := ADR(gAxMaster);
	fbMpAxisBasic_Master.Parameters := ADR(MasterPar);
	fbMpAxisBasic_Master();
	
	fbMpAxisBasic_Slave.MpLink := ADR(gAxSlave);
	fbMpAxisBasic_Slave.Parameters := ADR(SlavePar);
	fbMpAxisBasic_Slave();
	
	fbCamAutomatCommand.Deceleration := SlavePar.Deceleration;
	fbCamAutomatCommand.Slave := ADR(gAxSlave);
	fbCamAutomatCommand();
	
	fbCalcCamFromSections.CamDataAddress := ADR(CamData);
	fbCalcCamFromSections.CamSectionsAddress := ADR(CamSections);
	fbCalcCamFromSections();
	
	fbCamPrepare.Axis := ADR(gAxSlave);
	fbCamPrepare.Cam.DataAdress := ADR(CamData);
	fbCamPrepare();
	
	fbCamAutomatGetPar.CamAutomat.DataAddress := ADR(CamAutPar);
	fbCamAutomatGetPar.Slave := ADR(gAxSlave);
	fbCamAutomatGetPar();
	
	fbCamAutomatSetPar.CamAutomat := fbCamAutomatGetPar.CamAutomat;
	fbCamAutomatSetPar.Slave := ADR(gAxSlave);
	fbCamAutomatSetPar();
	
END_PROGRAM

PROGRAM _EXIT
	
	fbMpAxisBasic_Master(Power := FALSE);
	fbMpAxisBasic_Master(Enable := FALSE);
	
	fbMpAxisBasic_Slave(Power := FALSE);
	fbMpAxisBasic_Slave(Enable := FALSE);
	
END_PROGRAM

